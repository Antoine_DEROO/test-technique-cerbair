import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-film',
  templateUrl: './film.component.html',
  styleUrls: ['./film.component.css']
})
export class FilmComponent implements OnInit {

  title : string;
  episode_id : string;
  opening_crawl : string;
  director : string;
  producer : string;
  release_date : string;
  species : any[];
  starships : any[];
  vehicles : any[];
  characters : any[];
  planets : any[];
  url : string;
  created : string;
  edited : string;

  characters_output : {name:string, id:string}[] = [];
  species_output : {name:string, id:string}[] = [];
  starships_output : {name:string, id:string}[] = [];
  vehicles_names : string[] = [];
  films_names : string[] = [];
  planets_output : {name:string, id:string}[] = [];


  constructor(private route: ActivatedRoute, private http: HttpClient) { }

  ngOnInit() {
    const id = this.route.snapshot.params.id;
    this.http.get(
      'http://swapi.dev/api/films/'+(id)+'/',
      {responseType: 'json'})
      .subscribe((data: JSON) => {
          console.log(data);
          this.title = data['title'];
          this.episode_id = data['episode_id'];
          this.opening_crawl = data['opening_crawl'];
          this.director = data['director'];
          this.producer = data['producer'];
          this.release_date = data['release_date'];
          this.characters = data['characters']
          this.planets = data['planets'];
          this.species = data['species'];
          this.starships = data['starships'];
          this.vehicles = data['vehicles'];
          this.url = data['url'];
          this.created = data['created'];
          this.edited = data['edited'];

          for (let char of this.characters) {
            this.http.get(
              char,
              {responseType: 'json'})
              .subscribe((char_json: JSON) => {
                this.films_names.push(char_json['title']);
              });
          }
          console.log(this.films_names);
          // for (let specie of this.species) {
          //   this.http.get(
          //     specie,
          //     {responseType: 'json'})
          //     .subscribe((specie_json: JSON) => {
          //       this.species_names.push(specie_json['name']);
          //     });
          // }
          // for (let starship of this.starships) {
          //   this.http.get(
          //     starship,
          //     {responseType: 'json'})
          //     .subscribe((starship_json: JSON) => {
          //       this.starships_names.push(starship_json['name']);
          //     });
          // }
          for (let vehicle of this.vehicles) {
            this.http.get(
              vehicle,
              {responseType: 'json'})
              .subscribe((vehicle_json: JSON) => {
                this.vehicles_names.push(vehicle_json['name']);
              });
          }

        }
      );
  }

}
