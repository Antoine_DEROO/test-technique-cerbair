import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.css']
})



export class PeopleComponent implements OnInit {

  id : string;
  name : string;
  birth_year : string;
  eye_color : string;
  gender : string;
  hair_color : string;
  height : string;
  mass : string;
  skin_color : string;
  homeworld : string;
  films : string[];
  species : string[];
  starships : string[];
  vehicles : string[];
  url : string;
  created : string;
  edited : string;

  // name : name, id : id


  films_output : {name:string, id:string}[] = [];
  homeworld_output : {name:string, id:string} = {name:"noname",id:"-1"};
  species_output : {name:string, id:string}[] = [];
  starships_output : {name:string, id:string}[] = [];
  vehicles_names : string[] = [];




  constructor(private route: ActivatedRoute, private http: HttpClient) { }

  ngOnInit() {
    const id = this.route.snapshot.params.id;

    console.log("init people");
    this.http.get(
      'http://swapi.dev/api/people/'+id+'/',
      {responseType: 'json'})
      .subscribe((data: JSON) => {
          console.log("name : "+this.name);
          this.name = data['name'];
          this.id = '1';
          this.birth_year = data['birth_year'];
          this.eye_color = data['eye_color'];
          this.gender = data['gender'];
          this.hair_color = data['hair_color'];
          this.height = data['height'];
          this.mass = data['mass'];
          this.skin_color = data['skin_color'];
          this.homeworld = data['homeworld'];
          this.films = data['films'];
          this.species = data['species'];
          this.starships = data['starships'];
          this.vehicles = data['vehicles'];
          this.url = data['url'];
          this.created = data['created'];
          this.edited = data['edited'];

          console.log("films url");
          console.log(this.films);
          for (let film of this.films) {
            this.http.get(
              film,
              {responseType: 'json'})
              .subscribe((film_json: JSON) => {
                const list = film.split('/');
                this.films_output.push({'name':film_json['title'],'id':list[list.length-2]});
              });
          }
        for (let specie of this.species) {
          this.http.get(
            specie,
            {responseType: 'json'})
            .subscribe((specie_json: JSON) => {
              const list = specie.split('/');
              this.species_output.push({'name':specie_json['name'],'id':list[list.length-2]});
            });
        }
        for (let starship of this.starships) {
          this.http.get(
            starship,
            {responseType: 'json'})
            .subscribe((starship_json: JSON) => {
              const list = starship.split('/');
              this.starships_output.push({'name':starship_json['name'],'id':list[list.length-2]});
            });
        }
        for (let vehicle of this.vehicles) {
          this.http.get(
            vehicle,
            {responseType: 'json'})
            .subscribe((vehicle_json: JSON) => {
              this.vehicles_names.push(vehicle_json['name']);
            });
        }
        this.http.get(
          this.homeworld,
          {responseType: 'json'})
          .subscribe((homeworld_json: JSON) => {
              this.homeworld_output['name'] = homeworld_json['name'];
              const list = this.homeworld.split('/');
              this.homeworld_output['id'] = list[list.length-2];
            }
          );
        }
      );
  }

}
