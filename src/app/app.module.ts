import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PeopleComponent } from './people/people.component';
import {RouterModule, Routes} from "@angular/router";
import { FourOhFourComponent } from './four-oh-four/four-oh-four.component';
import { MainComponent } from './main/main.component';
import {HttpClientModule} from "@angular/common/http";
import { PlanetComponent } from './planet/planet.component';
import { FilmComponent } from './film/film.component';
import { StarshipComponent } from './starship/starship.component';
import { VehicleComponent } from './vehicle/vehicle.component';
import { SpecieComponent } from './specie/specie.component';

const appRoutes: Routes = [
  { path: 'films/:id', component: FilmComponent },
  { path: 'people/:id', component: PeopleComponent },
  { path: 'planets/:id', component: PlanetComponent },
  { path: 'starships/:id', component: StarshipComponent },
  { path: 'vehicles/:id', component: VehicleComponent },
  { path: 'species/:id', component: SpecieComponent },
  { path: 'main', component: MainComponent },
  { path: '', pathMatch:'full', redirectTo: 'people/1' },
  { path: 'not-found', component: FourOhFourComponent },
  { path: '**', redirectTo: 'not-found' }
];

@NgModule({
  declarations: [
    AppComponent,
    PeopleComponent,
    FourOhFourComponent,
    MainComponent,
    PlanetComponent,
    FilmComponent,
    StarshipComponent,
    VehicleComponent,
    SpecieComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
