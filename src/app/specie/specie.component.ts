import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-specie',
  templateUrl: './specie.component.html',
  styleUrls: ['./specie.component.css']
})
export class SpecieComponent implements OnInit {

  name : string;
  classification : string;
  designation : string;
  average_height : string;
  average_lifespan : string;
  eye_colors : string;
  hair_colors : string;
  skin_colors : string;
  language : string;
  homeworld : string;
  people : string;
  films : string;
  url : string;
  created : string;
  edited : string;

  films_output : {name:string, id:string}[] = [];
  people_output : {name:string, id:string}[] = [];
  homeworld_output : {name:string, id:string} = {name:"noname",id:"-1"};
  constructor(private route: ActivatedRoute, private http: HttpClient) { }

  ngOnInit() {
    const id = this.route.snapshot.params.id;
    this.http.get(
      'http://swapi.dev/api/species/'+(id)+'/',
      {responseType: 'json'})
      .subscribe((data: JSON) => {
        console.log(data);
        this.classification = data['classification'];
        this.designation = data['designation'];
        this.average_height = data['average_height'];
        this.average_lifespan = data['average_lifespan'];
        this.eye_colors = data['eye_colors'];
        this.hair_colors = data['hair_colors'];
        this.skin_colors = data['skin_colors'];
        this.language = data['language'];

        this.http.get(
          this.homeworld,
          {responseType: 'json'})
          .subscribe((homeworld_json: JSON) => {
              this.homeworld_output['name'] = homeworld_json['name'];
              const list = this.homeworld.split('/');
              this.homeworld_output['id'] = list[list.length-2];
            }
          );
        for (let film of this.films) {
          this.http.get(
            film,
            {responseType: 'json'})
            .subscribe((film_json: JSON) => {
              const list = film.split('/');
              this.films_output.push({'name':film_json['title'],'id':list[list.length-2]});
            });
        }
        for (let pers of this.people) {
          this.http.get(
            pers,
            {responseType: 'json'})
            .subscribe((pers_json: JSON) => {
              const list = pers.split('/');
              this.people_output.push({'name':pers_json['title'],'id':list[list.length-2]});
            });
        }
      });
  }

}
