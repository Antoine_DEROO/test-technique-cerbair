import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-planet',
  templateUrl: './planet.component.html',
  styleUrls: ['./planet.component.css']
})
export class PlanetComponent implements OnInit {

  name : string
  diameter : string;
  rotation_period : string;
  orbital_period : string;
  gravity : string;
  population : string;
  climate : string;
  terrain : string;
  surface_water : string;
  residents : string[];
  films : string[];
  url : string
  created : string
  edited : string


  films_output : {name:string, id:string}[] = [];
  residents_output : {name:string, id:string}[] = [];

  constructor(private route: ActivatedRoute, private http: HttpClient) { }

  ngOnInit() {
    const id = this.route.snapshot.params.id;

    console.log("init planets");
    this.http.get(
      'http://swapi.dev/api/planets/'+id+'/',
      {responseType: 'json'})
      .subscribe((data: JSON) => {
        console.log("name : " + this.name);
        this.name = data['name'];
        this.diameter = data['diameter'];
        this.rotation_period = data['rotation_period'];
        this.orbital_period = data['orbital_period'];
        this.gravity = data['gravity'];
        this.population = data['population'];
        this.climate = data['climate'];
        this.terrain = data['terrain'];
        this.surface_water = data['surface_water'];
        this.residents = data['residents'];
        this.url = data['url'];
        this.created = data['created'];
        this.edited = data['edited'];

        for (let film of this.films) {
          this.http.get(
            film,
            {responseType: 'json'})
            .subscribe((film_json: JSON) => {
              const list = film.split('/');
              this.films_output.push({'name': film_json['title'], 'id': list[list.length - 2]});
            });
        }
        for (let resident of this.residents) {
          this.http.get(
            resident,
            {responseType: 'json'})
            .subscribe((resident_json: JSON) => {
              const list = resident.split('/');
              this.residents_output.push({'name': resident_json['name'], 'id': list[list.length - 2]});
            });
        }

      });
  }

}
