# Starwars

## lancement de l'app

Placez-vous à la racine du projet, et lancez 'ng serve'

## Navigation dans l'app

Si on se connecte à 'http://localhost:4200/`, on voit la page de Luc Skywalker par défaut.
Certains liens sont cliquables vers d'autres pages, comme les 4 films ou bien la planète Tatooine.

On peut se connecter à toutes les ressources de type planètes, personnages, et films en entrant le lien http://localhost:4200/type/id
avec type = {people, planets ou films}
et id un nombre supérieur à 1

## Elements restant à implemeter

L'application est incomplète pour le moment. Il faudrait coder la page de présentation de tous les types de ressource, alors  qu'on n'a accès pour l'instant qu'aux planètes, personnages, et films.

La barre de recherche n'est pas disponible, on pourrait en rajouter une qui effectuerait une requête http sur tous les types de ressource en comparant l'entrée de la recherche avec les différents attributs de chaques objets.

## Outils utilisés : 

<p>IDE : Webstorm</p>
Modules supplémentaires : 
<ul>
    <li>HttpClientModule</li>
    <li>RouterModule</li>
    <li>AppRoutingModule</li>
</ul>
